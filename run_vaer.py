import argparse
import torch
import tqdm
from src import utils as ut
from src.models.vaer import VAER
from src.train import train
from pprint import pprint
import torch.utils.data as data_utils
import pandas as pd

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--comps_selection_metrics_dim',        type=int, default=166,     help="dim of comps selection metrics")
parser.add_argument('--regression_metrics_dim',        type=int, default=19,     help="dim of regression metrics")
parser.add_argument('--gw',         type=int, default=1,     help="Weight on the generative terms")
parser.add_argument('--rw',         type=int, default=100,   help="Weight on the regressor term")
parser.add_argument('--epoch_max',  type=int, default=1000, help="Number of training epochs")
parser.add_argument('--epoch_save', type=int, default=100, help="Save model every n epoch")
parser.add_argument('--nn',         type=str, default='enc_dec_reg_v2',     help="Which model to use")
parser.add_argument('--batch',      type=int, default=100,     help="Mini batch size")
parser.add_argument('--final',      type=int, default=0,     help="Final training, with train and val data sets")
args = parser.parse_args()
layout = [
    ('model={:s}',  'vaer'),
    ('nn={:s}', args.nn),
    ('gw={:03d}', args.gw),
    ('rw={:03d}', args.rw),
    ('batch={:04d}', args.batch)
]
model_name = '_'.join([t.format(v) for (t, v) in layout])
pprint(vars(args))
print('Model name:', model_name)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

train_set_df, val_set_df, test_set_df = ut.get_train_val_test_datasets()
train_set_df = train_set_df.drop(['title_lower'], axis=1)
val_set_df = val_set_df.drop(['title_lower'], axis=1)

if args.final:
    train_plus_val_df = pd.concat([train_set_df,val_set_df], ignore_index=True)
    train_set = torch.tensor(train_plus_val_df.values, dtype=torch.float).to(device)
else:
    train_set = torch.tensor(train_set_df.values, dtype=torch.float32).to(device)

train_dataset = data_utils.TensorDataset(train_set[:, :-1], train_set[:, -1].unsqueeze(1))

metrics_loader = data_utils.DataLoader(train_dataset, batch_size=args.batch, shuffle=True)

vaer = VAER(comps_selection_metrics_dim=args.comps_selection_metrics_dim, regression_metrics_dim=args.regression_metrics_dim, nn=args.nn, gen_weight=args.gw, regressor_weight=args.rw).to(device)

writer = ut.prepare_writer(model_name, overwrite_existing=True)
train(model=vaer,
      train_loader=metrics_loader,
      device=device,
      tqdm=tqdm.tqdm,
      writer=writer,
      epoch_max=args.epoch_max,
      epoch_save=args.epoch_save)



