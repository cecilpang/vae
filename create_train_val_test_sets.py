from src import utils as ut

train_set_df, val_set_df, test_set_df, combined_df = \
    ut.create_train_val_test_sets('data/comps_selection_metrics.csv', 'data/regression_metrics.csv', 20, 20)

train_titles = set(train_set_df['title_lower'])
val_titles = set(val_set_df['title_lower'])
test_titles = set(test_set_df['title_lower'])

print('train set: ', len(train_titles))
print('val set: ', len(val_titles))
print(val_titles)
print('test set: ', len(test_titles))
print(test_titles)