import unittest
import src.utils as ut


class TestTrainValTestSets(unittest.TestCase):
    def test_create_data_sets(self):
        val_size = 30
        test_size = 30

        train_set_df, val_set_df, test_set_df, combined_df = \
            ut.create_train_val_test_sets('data/comps_selection_metrics.csv', 'data/regression_metrics.csv', val_size, test_size)

        combined_rows, _ = combined_df.shape
        train_rows, _ = train_set_df.shape
        val_rows, _ = val_set_df.shape
        test_rows, _ = test_set_df.shape

        self.assertEqual(combined_rows, train_rows + val_rows + test_rows)

        train_titles = set(train_set_df['title_lower'])
        val_titles = set(val_set_df['title_lower'])
        test_titles = set(test_set_df['title_lower'])
        combined_titles = set(combined_df['title_lower'])

        self.assertFalse(train_titles & val_titles)
        self.assertFalse(train_titles & test_titles)
        self.assertFalse(val_titles & test_titles)

        self.assertTrue(combined_titles == train_titles.union(val_titles).union(test_titles))

        self.assertEqual(len(val_titles), val_size)
        self.assertEqual(len(test_titles), test_size)

        print('train set: ', len(train_titles))
        print('val set: ', len(val_titles))
        print(val_titles)
        print('test set: ', len(test_titles))
        print(test_titles)

        return


if __name__ == '__main__':
    unittest.main()
