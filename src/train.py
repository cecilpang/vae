import numpy as np
from src import utils as ut
from torch import optim


def train(model, train_loader, device, tqdm, writer, epoch_max=np.inf, epoch_save=np.inf, reinitialize=False):

    if reinitialize:
        model.apply(ut.reset_weights)

    optimizer = optim.Adam(model.parameters(), lr=1e-3)

    with tqdm(total=epoch_max) as pbar:
        for epoch in range(1, epoch_max+1):
            for batch_idx, (X_train, y_train) in enumerate(train_loader):

                optimizer.zero_grad()

                X_encoder, X_regressor = ut.split_comps_selection_regression_metrics(X_train)

                loss, summaries = model.loss(X_encoder, X_regressor, y_train)
                acc = summaries['reg/mse']

                loss.backward()
                optimizer.step()

            # progress bar
            pbar.set_postfix(epoch=epoch, loss='{:.2e}'.format(loss), acc='{:.2e}'.format(acc))
            pbar.update(1)

            # Save model
            if epoch % epoch_save == 0:
                ut.save_model_by_name(model, epoch)




