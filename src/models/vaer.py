import torch
from src import utils as ut
from src.models import nns
from torch import nn


class VAER(nn.Module):
    def __init__(self, comps_selection_metrics_dim, regression_metrics_dim, nn, name='vaer', z_dim=8, gen_weight=1, regressor_weight=100):
        super().__init__()
        self.nn = nn
        self.name = name
        self.comps_selection_metrics_dim = comps_selection_metrics_dim
        self.regression_metrics_dim = regression_metrics_dim
        self.z_dim = z_dim
        self.gen_weight = gen_weight
        self.regressor_weight = regressor_weight
        # Small note: unfortunate name clash with torch.nn
        # nn here refers to the specific architecture file found in
        # src/models/nns/*.py
        nn = getattr(nns, nn)
        self.enc = nn.Encoder(self.comps_selection_metrics_dim, self.regression_metrics_dim, self.z_dim)
        self.dec = nn.Decoder(self.comps_selection_metrics_dim, self.z_dim)

        # Set prior as fixed parameter attached to Module
        self.z_prior_m = torch.nn.Parameter(torch.zeros(1), requires_grad=False)
        self.z_prior_v = torch.nn.Parameter(torch.ones(1), requires_grad=False)
        self.z_prior = (self.z_prior_m, self.z_prior_v)

    # L(x) = −KL(q(s|x) || p(s)) + Eq(z|x)[log(x|z)] − Eq(s|x)[KL(q(z|x) || p(z))]
    # Note: p(z)
    def negative_elbo_bound(self, X_encoder, X_regressor):
        """
        Computes the Evidence Lower Bound, KL and, Reconstruction costs
        Note that nelbo = kl + rec
        Outputs should all be scalar

        Args:
            x: tensor: (batch, dim): Observations

        Returns:
            nelbo: tensor: (): Negative evidence lower bound
            kl: tensor: (): ELBO KL divergence to prior
            rec: tensor: (): ELBO Reconstruction term
        """

        m, v, y_logits = self.enc.encode_and_reg(X_encoder, X_regressor)
        z = ut.sample_gaussian(m, v)
        logits = self.dec.decode(z)
        kl = ut.kl_normal(m, v, self.z_prior[0], self.z_prior[1])
        rec = ut.mse(input=logits, target=X_encoder).sum(axis=1)
        nelbo = kl + rec
        nelbo, kl, rec = nelbo.mean(), kl.mean(), rec.mean()

        return nelbo, kl, rec, y_logits

    # L(x) = −KL(q(s|x) || p(s)) + Eq(z|x)[log(x|z)] − Eq(s|x)[KL(q(z|x) || p(z|s))]
    # Note: p(z|s)
    def negative_elbo_bound_v2(self, X_encoder, X_regressor):
        """
        Computes the Evidence Lower Bound, KL and, Reconstruction costs
        Note that nelbo = kl + rec
        Outputs should all be scalar

        Args:
            x: tensor: (batch, dim): Observations

        Returns:
            nelbo: tensor: (): Negative evidence lower bound
            kl: tensor: (): ELBO KL divergence to prior
            rec: tensor: (): ELBO Reconstruction term
        """

        z_m, z_v, z_hat_m, z_hat_v, y_m, y_v, y = self.enc.encode_and_reg(X_encoder, X_regressor)

        z = ut.sample_gaussian(z_m, z_v)
        logits = self.dec.decode(z)

        kl = ut.kl_normal(z_m, z_v, z_hat_m, z_hat_v)  # KL(q(z|x) || p(z|s)
        rec = ut.mse(input=logits, target=X_encoder).sum(axis=1)

        nelbo = kl + rec
        nelbo, kl, rec = nelbo.mean(), kl.mean(), rec.mean()

        return nelbo, kl, rec, y

    def loss(self, X_encoder, X_regressor, y_regressor):
        if self.gen_weight > 0:
            if self.nn == 'enc_dec_reg':
                nelbo, kl, rec, y_hat = self.negative_elbo_bound(X_encoder, X_regressor)
            elif self.nn == 'enc_dec_reg_v2':
                nelbo, kl, rec, y_hat = self.negative_elbo_bound_v2(X_encoder, X_regressor)
            else:
                raise ValueError('nn not valid.')
        else:
            nelbo, kl, rec = [0] * 3
            y_hat = self.enc.reg(X_encoder, X_regressor)

        reg_mse = ut.mse(input=y_hat, target=y_regressor)
        reg_mse = reg_mse.mean()
        loss = self.gen_weight * nelbo + self.regressor_weight * reg_mse

        summaries = dict((
            ('train/loss', nelbo),
            ('gen/elbo', -nelbo),
            ('gen/kl_z', kl),
            ('gen/rec', rec),
            ('reg/mse', reg_mse)
        ))

        return loss, summaries

    def validate_reg_output(self, X, y):
        y_hat, reg_mse, z_m, z_v = self.enc.validate_reg_output(X, y)
        return y_hat, reg_mse, z_m, z_v

    def sample_sigmoid(self, batch):
        z = self.sample_z(batch)
        return self.compute_sigmoid_given(z)

    def compute_sigmoid_given(self, z):
        logits = self.dec.decode(z)
        return torch.sigmoid(logits)

    def sample_z(self, batch):
        return ut.sample_gaussian(
            self.z_prior[0].expand(batch, self.z_dim),
            self.z_prior[1].expand(batch, self.z_dim))

    def sample_x(self, batch):
        z = self.sample_z(batch)
        return self.sample_x_given(z)

    def sample_x_given(self, z):
        return torch.bernoulli(self.compute_sigmoid_given(z))