from src import utils as ut
from torch import nn
import torch


# latent variables NOT constrained by output of regressor
class Encoder(nn.Module):
    def __init__(self, comps_selection_metrics_dim, regression_metrics_dim, z_dim):
        super().__init__()
        self.comps_selection_metrics_dim = comps_selection_metrics_dim
        self.z_dim = z_dim
        self.shared_layers_dim = 64
        # first 2 layers shared by encoder and regressor
        self.shared_layers = nn.Sequential(
            nn.Linear(self.comps_selection_metrics_dim, 128),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(128, self.shared_layers_dim),
            nn.Tanh(),
        )

        self.encoder = nn.Sequential(
            nn.Linear(self.shared_layers_dim, 2 * z_dim)
        )

        self.regressor = nn.Sequential(
            nn.Linear(self.shared_layers_dim + regression_metrics_dim, 64),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(32, 1)
        )
        # self.net = nn.Sequential(
        #     nn.Linear(x_dim, 64),
        #     nn.Tanh(),
        #     nn.Dropout(0.1),
        #     nn.Linear(64, 32),
        #     nn.Tanh(),
        #     nn.Linear(32, 2 * z_dim),
        # )

    # forward pass of encoder and regressor
    def encode_and_reg(self, X_encoder, X_regressor):
        shared = self.shared_layers(X_encoder)
        encoded = self.encoder(shared)
        m, v = ut.gaussian_parameters(encoded, dim=1)

        X_combined = torch.cat((shared, X_regressor), dim=1)
        y_hat = self.regressor(X_combined)

        return m, v, y_hat

    def validate_reg_output(self, X, y):
        X_encoder, X_regressor = ut.split_comps_selection_regression_metrics(X)
        m, v, y_hat = self.encode_and_reg(X_encoder, X_regressor)
        reg_mse = ut.mse(input=y_hat, target=y)
        return y_hat, reg_mse, m, v


class Decoder(nn.Module):
    def __init__(self, x_dim, z_dim):
        super().__init__()
        self.z_dim = z_dim
        self.x_dim = x_dim
        self.net = nn.Sequential(
            nn.Linear(z_dim, 32),
            nn.Tanh(),
            nn.Linear(32, 64),
            nn.Tanh(),
            nn.Linear(64, x_dim)
        )

    def decode(self, z):
        return self.net(z)
