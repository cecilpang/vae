from src import utils as ut
from torch import nn
import torch


# regressor only. Same as encoder+regressor except no encoder
class Encoder(nn.Module):
    def __init__(self, comps_selection_metrics_dim, regression_metrics_dim, z_dim):
        super().__init__()
        self.comps_selection_metrics_dim = comps_selection_metrics_dim
        self.z_dim = z_dim
        self.shared_layers_dim = 64
        # first 2 layers
        self.shared_layers = nn.Sequential(
            nn.Linear(self.comps_selection_metrics_dim, 128),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(128, self.shared_layers_dim),
            nn.Tanh(),
        )

        self.regressor = nn.Sequential(
            nn.Linear(self.shared_layers_dim + regression_metrics_dim, 64),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(64, 32),
            nn.Tanh(),
            nn.Dropout(p=0.2),
            nn.Linear(32, 1)
        )

    # forward pass of regressor
    def reg(self, X_encoder, X_regressor):
        shared = self.shared_layers(X_encoder)
        X_combined = torch.cat((shared, X_regressor), dim=1)
        y_hat = self.regressor(X_combined)

        return y_hat

    def validate_reg_output(self, X, y):
        X_encoder, X_regressor = ut.split_comps_selection_regression_metrics(X)
        y_hat = self.reg(X_encoder, X_regressor)
        reg_mse = ut.mse(input=y_hat, target=y)
        return y_hat, reg_mse, 0, 0

# Not used
class Decoder(nn.Module):
    def __init__(self, x_dim, z_dim):
        super().__init__()
        self.z_dim = z_dim
        self.x_dim = x_dim
        self.net = nn.Sequential(
            nn.Linear(z_dim, 32),
            nn.Tanh(),
            nn.Linear(32, 64),
            nn.Tanh(),
            nn.Linear(64, x_dim)
        )

    def decode(self, z):
        return self.net(z)